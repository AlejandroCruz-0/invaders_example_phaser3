var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,

    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 200 },
            debug: false
        }
    },

    scene: {
        preload: preload,
        create: create,
        update: update,
        
    }
};

var player; //Jugador
var aliens; //Aliens
var bullet; //bala Jugador
var InvaderBullet;//bala alien
var eventoDeTiempo; //para crear gatillo de tiempo
var bulletTime = 0;//var para seguir el tiempo del juego
var cursors;//variable para crear teclas basicas de input como las flechas de movimiento
var fireButton; //Para guardar boton de disparo
var restartButton //boton para reiniciar
var starfield; //Para guardar fondo
var score = 0; //Guarda el puntaje
var scoreString = ''; //El mesnsaje a mostrar junto al puntaje
var scoreText; //Crea el texto en la pantalla
var lives; //vidas Jugador
var stateText; //Para Mostar mensaje cuando gana || pierde
var playerBullets; //Grupo Balas Jugador
var alienBullets;//Grupo Balas aliens
var container;//var para el container que va a contener nuestros aliens
var debug_scene;
var game = new Phaser.Game(config);


function preload() {

    this.load.image('bullet', 'assets/bullet.png');
    this.load.image('enemyBullet', 'assets/enemy-bullet.png');
    this.load.spritesheet('invader', 'assets/invader32x32x4.png', { frameWidth: 32, frameHeight: 32 });
    this.load.image('ship', 'assets/player.png');
    this.load.spritesheet('kaboom', 'assets/explode.png', { frameWidth: 128, frameHeight: 128 });
    this.load.image('starfield', 'assets/starfield.png');


}


function create() {
    console.log("create function this scope");
    console.log(this);


    //configurar bordes del mundo para que tengan colision
    this.physics.world.setBoundsCollision(true, true, true, true);

    //Fondo de estrellas
    starfield = this.add.tileSprite(0, 0, 800, 600, 'starfield').setOrigin(0, 0);


    player = this.physics.add.image(400, 500, 'ship'); //Crea la nave usando el sistema de fisicas de Phaser 3
    player.body.setAllowGravity(false); //Desactiva la gravedad en el objeto nave
    player.setCollideWorldBounds(true) //Abilita colision con los border de la escena

    playerBullets = this.add.group(); //Creacion de grupo para balas jugador
    alienBullets = this.add.group();//Creacion de grupo para balas alien


    //animacion de invasores
    this.anims.create({
        key: 'fly',
        frames: this.anims.generateFrameNumbers('invader', { start: 0, end: 3 }),
        frameRate: 20,
        repeat: -1
    });


    //animacion de explosion
    this.anims.create({
        key: 'boom',
        frames: this.anims.generateFrameNumbers('kaboom', { start: 0, end: 15, first: 0 }),
        hideOnComplete: true,
        //repeat: 1
    });


    /*
    funcion para llamar aliens. se usa la funcion call() para llamar a la funcion createAliens
    Desde el scope de la funcion create de esta escena. De lo contrario tendria que acceder a la
    escena directamente usando game.scene.scenes[0] para poder usar funciones como fisicas.

    https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Function/call

    */
    createAliens.call(this);





    //  Puntaje
    scoreString = 'Score : ';
    scoreText = this.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });


    //  configuracion de Vidas
    lives = this.add.group();
    this.add.text(700, 10, 'Vidas : ', { font: '34px Arial', fill: '#fff' });

    //configuracion de vidas, se usa el mismo sprite para     
    for (var i = 0; i < 3; i++) {
        var ship = lives.create(800 - 70 + (30 * i), 60, 'ship');
        ship.angle = 90;
        ship.alpha = 0.9;
    }


    // Texto De estado de juego
    stateText = this.add.text(50, 150, '', { font: '60px Arial', fill: '#fff' });
    stateText.visible = false;



    //Controles de fechas de teclado
    cursors = this.input.keyboard.createCursorKeys();
    //El boton de disṕaro asignado a la tecla espacio
    fireButton = this.input.keyboard.addKey("SPACE");

    restartButton = this.input.keyboard.addKey("R");



    //funcion para destruir los objetos que tocan con los bordes del mundo
    this.physics.world.on('worldbounds', (body) => console.log("Destroyed" + body.gameObject.destroy()));


    //funcion para que los aliens disparen comentada
/*
    eventoDeTiempo = this.time.addEvent({
        loop: true,
        callback: aliensDisparan,
        callbackScope: this,
        delay: 3000
    })*/



    //collider balas jugador con alien

    this.physics.add.collider(playerBullets, aliens, function (bullet, alien) {
        bullet.destroy();
        alien.destroy();


        //  Increase the score
        score += 20;
        scoreText.text = scoreString + score;
        bomb = this.add.sprite(alien.x + container.x, alien.y + container.y, "kaboom").play("boom");

        if (aliens.getChildren().length == 0) {
            score += 1000;
            scoreText.text = scoreString + score;

            textoGanaroPerder.call(this, true);





        }
    }, undefined, this);


    //collider balas alien con jugador
    this.physics.add.collider(alienBullets, player, function (alienBullet, player) {
        alienBullet.destroy();
        this.add.sprite(player.x, player.y, "kaboom").play("boom");


        if (lives.getChildren().length == 1) {
            //funcion para remover evento de tiempo en loop
            eventoDeTiempo.remove(false);
            alienTween.stop();
            textoGanaroPerder.call(this, false);

        } else {
            lives.getFirstAlive().destroy();
            alienTween.stop();
            container.x = 100;
            container.y = 50;
            alienTween.resume();
            player.x = 400;
            player.y = 500;

        }


    }, undefined, this);



    //collider entre aliens y jugador
    this.physics.add.collider(player, aliens, function (player, alien) {


        alien.destroy();
        if (lives.getChildren().length == 0) {
            //funcion para remove evento de tiempo en loop
            eventoDeTiempo.remove(false);
            alienTween.stop();
            textoGanaroPerder.call(this, false);

        } else {
            lives.getFirstAlive().destroy();
            alienTween.stop();
            container.x = 100;
            container.y = 50;
            alienTween.resume();

            player.x = 400;
            player.y = 500;
        }



    }, undefined, this);


}//fin de phaser create



function update() {



    //  Scroll the background -- Mover el Fondo 
    starfield.tilePositionY += 2;



    if (player.active) {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown) { player.body.velocity.x = -200; }
        else if (cursors.right.isDown) { player.body.velocity.x = 200; }

        //  Firing?
        if (fireButton.isDown) {
            fireBullet.call(this);

        }

        if (restartButton.isDown) { this.scene.restart(); }


        


    }

} //fin de funcion update de Phaser 3






//Funcion Para crear un evento de escucha cuando se hace un click
//reinica la escene Phaser
function reiniciaElJuego() {
    this.input.addListener('pointerdown', function (pointer) {

        this.scene.restart();

    }, this);
}




function fireBullet() {
    //Funcion Que dispara una bala al presionar espacio

    //  To avoid them being allowed to fire too fast we set a time limit
    //game.scene.scenes[0].sys.time.now

    if (this.scene.scene.time.now > bulletTime) {


        bullet = game.scene.scenes[0].physics.add.image(player.x, player.y - 8, 'bullet').setVelocity(0, -400).setCollideWorldBounds(true);
        bullet.body.setAllowGravity(false);
        bullet.body.onWorldBounds = true;
        playerBullets.add(bullet);
        bulletTime = game.scene.scenes[0].sys.time.now + 200;



    }

}


function aliensDisparan() {

    //Tomamos un alien vivo al azar
    randomAlien = aliens.getChildren()[Phaser.Math.Between(0, aliens.getChildren().length - 1)]
    if (randomAlien) {
        InvaderBullet = this.physics.add.image(randomAlien.x + container.x, randomAlien.y + container.y, "enemyBullet").setCollideWorldBounds(true);
        InvaderBullet.body.setAllowGravity(false);
        InvaderBullet.body.onWorldBounds = true;
        this.physics.moveToObject(InvaderBullet, player, 150);
        alienBullets.add(InvaderBullet);

    }

}


function createAliens() {


    //Los Aliens
    //Para registrar colision los aliens tienen que estar en un grupo de fisica Phaser.
    aliens = this.physics.add.group({

    });

    for (let y = 0; y < 4; y++) {
        for (let x = 0; x < 10; x++) {
            let alien = aliens.create(x * 48, y * 50, 'invader');
            alien.setOrigin(0.5, 0.5);

            alien.anims.play('fly');
            alien.body.moves = false;
        }
    }

    //Los grupos en Phaser 3 no tienen metodos de ajuste de posicion asi que
    //tenemos que usar los contenedores de Phaser para ajustar la posicion del grupo de aliens.
    container = this.add.container(100, 50);
    container.add(aliens.getChildren());

    //Los tween de Phaser 3 permite el movimientos de un objeto o grupo.

    //movimiento alien


    alienTween = this.tweens.add({
        targets: container, //objeto que contiene el grupo de aliens
        x: 200,

        duration: 2000,
        loop: -1,
        yoyo: true,
        ease: 'Linear.easeInOut',
        onYoyo: function () { container.y += 200 },
    })



}



function textoGanaroPerder(estado) {


    if (estado) {
        stateText.visible = true;
        stateText.text = "Has Ganado,\npresiona r para reiniciar";
        reiniciaElJuego.call(this);
    }

    if (!estado) {
        stateText.visible = true;
        stateText.text = "Has Perdido,\npresiona r para reiniciar";
        player.body.enable = false;
        reiniciaElJuego.call(this);

    }

}

